﻿using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using SunatQuery.Objects;
using SunatQuery.TextCleaner;
using SunatQuery.Util;

namespace SunatQuery
{
    public class QueryEinvoice : ISunatQuery<DocumentoElectronico, QueryEinvoice.Response>
    {
        public enum Response
        {
            ErrorConexion,

            Desconocido,

            DocumentoValido,

            DocumentoNoInformado,

            DocumentoDadoBaja,
        };

        const int Timeout = 5 * 1000;

        private CookieContainer _cookieContainer;

        public QueryEinvoice()
        {
            _cookieContainer = new CookieContainer();

            TextClear = new TesseractTextCleaner();
        }

        public Response Query(DocumentoElectronico query)
        {
            int intentos = 3;

            ImageCaptcha imageCaptcha = new ImageCaptcha(_cookieContainer);

            string captcha = string.Empty;

            do
            {
                Image image = imageCaptcha.FromUrl(this.UrlCaptcha);

                captcha = imageCaptcha.GetText(image);

                intentos--;

            } while (captcha.Length < 4 && intentos > 0);

            captcha = captcha.ToUpper();

            string parameters = "?accion=CapturaCriterioValidez&num_ruc={0}&tipocomprobante={1}&num_serie={2}&num_comprob={3}&fec_emision={4}&codigo={5}";

            parameters = string.Format(parameters, query.RucEmisor, query.Tipo, query.Serie, query.Correlativo, query.FechaEmision.ToString("dd/MM/yyyy"), captcha);

            string url = this.Url + parameters;

            string responseHtml = RequestHtml.GetHtml(url, _cookieContainer, Timeout);

            string pattern = "<td width=\"30%\" height=\"15px\" class=\"bgn\">(.*)<\\/td>";

            MatchCollection matches = Regex.Matches(responseHtml, pattern);

            for (int i = 0; i < matches.Count; i++)
            {
                string message = matches[i].Groups[1].Value;

                if (message.Contains("vÃ¡lido"))
                {
                    return Response.DocumentoValido;
                }
                else if (message.Contains("no ha sido informada"))
                {
                    return Response.DocumentoNoInformado;
                }
                else if (message.Contains("comunicada de BAJA"))
                {
                    return Response.DocumentoDadoBaja;
                }
                else
                {
                    return Response.Desconocido;
                }
            }

            return Response.ErrorConexion;
        }

        public ITextCleaner TextClear { get; set; }

        public string Url { get; set; }

        public string UrlCaptcha { get; set; }
    }
}