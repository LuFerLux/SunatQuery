﻿# Librería para consultas a sunat
### Escrita en c# y bajo métodos POST

La única dependencia es TesseractEngine3, debe ser agregada en el proyecto donde será usada la librería.

## Ejemplo de uso

1. Consulta de documentos electrónicos
	* Devuelve una respuesta indicando si el documento electrónico es un documento válido, si no ha sifo informado o si se encuentra dado de baja
	en SUNAT, si hubo un error de conexión o si la respuesta devuelta es desconocida.
    
	```csharp
	// Ejemplo.cs
    
	var query = new QueryEinvoice()
    {
        Url = "http://www.sunat.gob.pe/ol-ti-itconsvalicpe/ConsValiCpe.htm",
    
		UrlCaptcha = "http://www.sunat.gob.pe/ol-ti-itconsvalicpe/captcha?accion=image&nmagic=1",
    };
	
	var comprobante = new SunatQuery.Objects.DocumentoElectronico()
	{
		RucEmisor = "20102351038",
		
		Tipo = "06",
		
		Serie = "B111",
		
		Correlativo = "107182",
	
		FechaEmision = DateTime.Parse("11/10/2017"),
	};
	
	QueryEinvoice.Response response = query.Query(comprobante);

	```