﻿using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;
using tesseract;

namespace SunatQuery.Util
{
    public class ImageCaptcha
    {
        private TesseractProcessor _tesseract;

        private CookieContainer _cookieContainer;

        public ImageCaptcha(CookieContainer _cookieContainer)
        {
            this._cookieContainer = _cookieContainer;

            _tesseract = new ConfigOCR(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\SunatQuery\tessdata\").ApplyConfig("eng", 3);
        }

        public Image FromUrl(string url)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidarCertificado);

                HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create(url);

                myWebRequest.CookieContainer = this._cookieContainer;

                myWebRequest.Proxy = null;

                myWebRequest.Credentials = CredentialCache.DefaultCredentials;

                HttpWebResponse httpWebResponse = (HttpWebResponse)myWebRequest.GetResponse();

                Stream stream = httpWebResponse.GetResponseStream();

                return Image.FromStream(stream);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetText(Image image)
        {
            Bitmap bitmap = null;

            PictureBox pictureCapcha = new PictureBox();

            pictureCapcha.Image = image;

            pictureCapcha.SizeMode = PictureBoxSizeMode.AutoSize;

            bitmap = new Bitmap(pictureCapcha.Image, pictureCapcha.Width, pictureCapcha.Height);

            Image xImage = bitmap;

            _tesseract.Clear();

            _tesseract.ClearAdaptiveClassifier();

            string texto = _tesseract.Apply(xImage);

            return (texto);
        }

        private Boolean ValidarCertificado(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
