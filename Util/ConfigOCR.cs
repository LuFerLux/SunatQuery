﻿using System;
using tesseract;

namespace SunatQuery.Util
{
    public class ConfigOCR
    {
        private string _path;

        private TesseractProcessor tesseract = null;

        public ConfigOCR(string path)
        {
            this._path = path;


            tesseract = new TesseractProcessor();
        }

        public TesseractProcessor ApplyConfig(string lang, int ocrEngineMode)
        {
            bool success = tesseract.Init(_path, lang, ocrEngineMode);

            if (!success)
            {
                string message = "Error al inicializar tesseract.";

                message += string.Format("path:{0}, lang:{1}, ocrEngineMode:{2}", _path, lang, ocrEngineMode);

                throw new Exception(message);
            }

            tesseract.SetVariable("tessedit_pageseg_mode", "3");

            //System.Environment.CurrentDirectory = Path.GetFullPath(p1);

            return tesseract;
        }
    }
}
