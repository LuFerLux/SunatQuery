﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SunatQuery.Util
{
    public class RequestHtml
    {

        public static string GetHtml(string url, CookieContainer cookieContainer, int timeout = 5000)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0";

            httpWebRequest.CookieContainer = cookieContainer;

            httpWebRequest.Credentials = CredentialCache.DefaultCredentials;

            httpWebRequest.Timeout = timeout;

            httpWebRequest.Proxy = null;

            string responseHtml = null;

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

            using (var stream = httpWebResponse.GetResponseStream())
            {
                using (var stremReader = new StreamReader(stream, Encoding.Default))
                {
                    responseHtml = stremReader.ReadToEnd();
                }
            }

            httpWebResponse.Close();

            return responseHtml;
        }

    }
}
