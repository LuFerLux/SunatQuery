﻿using System;

namespace SunatQuery.Objects
{
    public class DocumentoElectronico
    {
        public DocumentoElectronico()
        {
            FechaEmision = DateTime.Today;
        }

        public string RucEmisor { get; set; }

        public string Tipo { get; set; }

        public string Serie { get; set; }

        public string Correlativo { get; set; }

        public DateTime FechaEmision { get; set; }
    }
}
