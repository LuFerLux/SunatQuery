﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunatQuery.TextCleaner
{
    public interface ITextCleaner
    {
        string Clear(string text);
    }
}
