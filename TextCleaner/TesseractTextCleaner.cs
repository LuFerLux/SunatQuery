﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunatQuery.TextCleaner
{
    public class TesseractTextCleaner : ITextCleaner
    {
        public string Clear(string text)
        {
            StringBuilder builder = new StringBuilder(text);

            builder.Replace(" ", "");

            builder.Replace("/", "");

            builder.Replace("\\", "");

            builder.Replace("(", "");

            builder.Replace(")", "");

            builder.Replace("[", "");

            builder.Replace("]", "");

            builder.Replace("|", "");

            builder.Replace(">", "");

            builder.Replace("<", "");

            builder.Replace("" + Convert.ToChar(34), "");

            builder.Replace(",", "");

            builder.Replace(".", "");

            builder.Replace(":", "");

            builder.Replace(";", "");

            builder.Replace("_", "");

            builder.Replace("-", "");

            builder.Replace("Ã", "");

            builder.Replace("Â", "");

            builder.Replace("Å", "");

            builder.Replace("€", "");

            builder.Replace("Î", "");

            builder.Replace("?", "");

            builder.Replace("¿", "");

            builder.Replace("@", "");

            builder.Replace("*", "");

            builder.Replace("Ï", "");

            builder.Replace("¬", "");

            builder.Replace("ƒ", "");

            builder.Replace("!", "");

            builder.Replace("¡", "");

            builder.Replace("$", "S");

            builder.Replace("‰", "");

            builder.Replace("'", "");

            builder.Replace("®", "");

            builder.Replace("‡", "");

            builder.Replace("Ž", "");

            builder.Replace("" + Convert.ToChar(10), "");

            builder.Replace("" + Convert.ToChar(13), "");

            builder.Replace("\r", "");

            builder.Replace("\n", "");

            return builder.ToString();
        }
    }
}